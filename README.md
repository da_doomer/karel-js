# Karel-JS

Live demo: [https://da_doomer.gitlab.io/karel-js/](https://da_doomer.gitlab.io/karel-js/).

A simple Karel DSL, interpreter and plotting functions.

## Development instructions

To run a local server:

```
$ npm install
$ npm run dev
```

## Deployment instructions

Run `$ npm run build` and deploy in your server of choice. A Gitlab CI/CD script can be found in [.gitlab-ci.yml](.gitlab-ci.yml).
