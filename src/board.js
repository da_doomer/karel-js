/**
 * Karel board.
 */

const RIGHT = "right";
const LEFT = "left";
const UP = "up";
const DOWN = "down";

class Board {
	/*
	 * A Karel Board with a robot position and a robot orientation, a dictionary
	 * mapping coordinates to marker numbers and a dictionary mapping coordinates
	 * to booleans representing whether the space is occupied by a wall.
	 *
	 * In this variation walls occupy a whole block.
	 */
	constructor() {
		this.marker_n_at = {};
		this.wall_at = {};
		this.robot_position = [0, 0];
		this.robot_orientation = RIGHT;
		this.touched_is = new Set([this.robot_position[0]]);
		this.touched_js = new Set([this.robot_position[1]]);
	}

	get_robot_position() {
		return this.robot_position;
	}

	set_robot_position([i, j]) {
		this.robot_position = [i, j];
		this.touched_is.add(i);
		this.touched_js.add(j);
	}

	get_robot_orientation(orientation) {
		return this.robot_orientation;
	}

	set_robot_orientation(orientation) {
		this.robot_orientation = orientation;
	}

	get_marker_n_at([i, j]) {
		if([i, j] in this.marker_n_at)
			return this.marker_n_at[[i, j]];
		return 0;
	}

	put_marker_at([i, j]) {
		if([i, j] in this.marker_n_at)
			this.marker_n_at[[i, j]] += 1;
		else
			this.marker_n_at[[i, j]] = 1;
		this.touched_is.add(i);
		this.touched_js.add(j);
	}

	pick_marker_at([i, j]) {
		if([i, j] in this.marker_n_at && this.marker_n_at[[i,j]] > 0)
			this.marker_n_at[[i, j]] -= 1;
		this.touched_is.add(i);
		this.touched_js.add(j);
	}

	is_wall([i, j]) {
		if([i, j] in this.wall_at)
			return this.wall_at[[i, j]];
		return false;
	}

	set_wall([i, j], value) {
		this.wall_at[[i, j]] = value;
		this.touched_is.add(i);
		this.touched_js.add(j);
	}

	get_min_i() {
		return Math.min(...this.touched_is);
	}

	get_max_i() {
		return Math.max(...this.touched_is);
	}

	get_min_j() {
		return Math.min(...this.touched_js);
	}

	get_max_j() {
		return Math.max(...this.touched_js);
	}

	get_active_coordinates() {
		let active_coordinates = [];
		let min_i = this.get_min_i();
		let max_i = this.get_max_i();
		let min_j = this.get_min_j();
		let max_j = this.get_max_j();
		for(let i = min_i; i <= max_i; i++)
		for(let j = min_j; j <= max_j; j++) {
			// Test if coordinate is active
			let active = false;
			if(this.is_wall([i, j]))
				active = true;
			if(this.get_marker_n_at([i, j]) > 0)
				active = true;
			if(i == this.get_robot_position()[0] && j == this.get_robot_position()[1])
				active = true;

			// If active, add to active coordinates
			if(active)
				active_coordinates = [...active_coordinates, [i, j]];
		}
		return active_coordinates;
	}
}

Board.random = function() {
	let board = new Board();
	board.set_wall([1, 1], true);
	board.set_wall([0, 1], true);
	board.set_wall([-1, 1], true);
	board.set_wall([-1, 0], true);
	board.set_wall([-1, -1], true);
	board.set_wall([0, -1], true);
	board.set_wall([1, -1], true);
	board.put_marker_at([0, 0]);
	board.put_marker_at([2, 0]);
	board.put_marker_at([2, 0]);
	board.put_marker_at([2, 2]);
	board.set_robot_position([2, 0]);
	board.set_robot_orientation(UP);
	return board;
}

export {
	Board,
	RIGHT,
	LEFT,
	DOWN,
	UP
};
