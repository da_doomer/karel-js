import { FrontIsClear } from "./front_is_clear.js";
import { FRONT_IS_CLEAR_TYPE } from "./front_is_clear.js";
import { LeftIsClear } from "./left_is_clear.js";
import { LEFT_IS_CLEAR_TYPE } from "./left_is_clear.js";
import { RightIsClear } from "./right_is_clear.js";
import { RIGHT_IS_CLEAR_TYPE } from "./right_is_clear.js";
import { MarkersPresent } from "./markers_present.js";
import { MARKERS_PRESENT_TYPE } from "./markers_present.js";
import { NoMarkersPresent } from "./no_markers_present.js";
import { NO_MARKERS_PRESENT_TYPE } from "./no_markers_present.js";
import { Not } from "./not.js";
import { NOT_TYPE } from "./not.js";
import { random_item } from "./dsl.js";

class Condition {}

Condition.from_json = function(json) {
	if(json.type == FRONT_IS_CLEAR_TYPE)
		return new FrontIsClear.from_json(json);
	if(json.type == LEFT_IS_CLEAR_TYPE)
		return new LeftIsClear.from_json(json);
	if(json.type == RIGHT_IS_CLEAR_TYPE)
		return new RightIsClear.from_json(json);
	if(json.type == MARKERS_PRESENT_TYPE)
		return new MarkersPresent.from_json(json);
	if(json.type == NO_MARKERS_PRESENT_TYPE)
		return new NoMarkersPresent.from_json(json);
	if(json.type == NOT_TYPE)
		return new Not.from_json(json);
}

Condition.random = function(max_recursion) {
	let classes = [FrontIsClear, LeftIsClear, RightIsClear, MarkersPresent, NoMarkersPresent, Not];
	let Class = random_item(classes);
	return Class.random(max_recursion-1);
}

export { Condition };
