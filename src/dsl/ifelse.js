import { Condition } from "./condition.js";
import { Statement } from "./statement.js";
import { INDENT_SIZE } from "./dsl.js";
import { indent } from "./dsl.js";
let IFELSE_TYPE = "ifelse";

class IfElse {
	constructor(condition, statement1, statement2) {
		this.condition = condition;
		this.statement1 = statement1;
		this.statement2 = statement2;
	}

	get_code() {
		return "if " + this.condition.get_code() + ":\n" + indent(this.statement1.get_code(), INDENT_SIZE) + "else:\n" + indent(this.statement2.get_code(), INDENT_SIZE);
	}

	transform(board, parameters, callback) {
		callback(this);
		if(this.condition.transform(board, parameters, callback))
			this.statement1.transform(board, parameters, callback);
		else
			this.statement2.transform(board, parameters, callback);
	}
}

IfElse.from_json = function(json) {
	return new IfElse(Condition.from_json(json.condition), Statement.from_json(json.statement1), Statement.from_json(json.statement2));
}

IfElse.random = function(max_recursion) {
	return new IfElse(Condition.random(max_recursion-1), Statement.random(max_recursion-1), Statement.random(max_recursion-1));
}

export { IfElse, IFELSE_TYPE };
