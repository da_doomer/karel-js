import { RIGHT, LEFT, UP, DOWN } from "../board.js";
let LEFT_IS_CLEAR_TYPE = "left_is_clear";

class LeftIsClear {
	constructor() {
		this.type = LEFT_IS_CLEAR_TYPE;
	}

	get_code() {
		return "left_is_clear()";
	}

	transform(board, parameters, callback) {
		callback(this);
		let i = board.get_robot_position()[0];
		let j = board.get_robot_position()[1];
		let new_i = i;
		let new_j = j;
		if(board.get_robot_orientation() == UP)
			new_i -= 1;
		else if(board.get_robot_orientation() == DOWN)
			new_i += 1;
		else if(board.get_robot_orientation() == RIGHT)
			new_j += 1;
		else if(board.get_robot_orientation() == LEFT)
			new_j -= 1;
		if(!board.is_wall([new_i, new_j]))
			return true;
		return false;
	}
}

LeftIsClear.from_json = function(json) {
	return new LeftIsClear();
}

LeftIsClear.random = function(max_recursion) {
	return new LeftIsClear();
}

export { LeftIsClear, LEFT_IS_CLEAR_TYPE };
