let MARKERS_PRESENT_TYPE = "markers_present";

class MarkersPresent {
	constructor() {
		this.type = MARKERS_PRESENT_TYPE;
	}

	get_code() {
		return "markers_present()";
	}

	transform(board, parameters, callback) {
		callback(this);
		if(board.get_marker_n_at(board.get_robot_position()))
			return true;
		return false;
	}
}

MarkersPresent.from_json = function(json) {
	return new MarkersPresent();
}

MarkersPresent.random = function(max_recursion) {
	return new MarkersPresent();
}

export { MarkersPresent, MARKERS_PRESENT_TYPE };
