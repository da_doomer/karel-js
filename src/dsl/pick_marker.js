let PICK_MARKER_TYPE = "pick_marker";

class PickMarker {
	constructor() {
		this.type = PICK_MARKER_TYPE;
	}

	get_code() {
		return "pick_marker()";
	}

	transform(board, parameters, callback) {
		callback(this);
		if(parameters.max_budget <= 0)
			return;
		if(board.get_marker_n_at(board.get_robot_position()) > 0)
			board.pick_marker_at(board.get_robot_position());
		parameters.max_budget -= 1;
	}
}

PickMarker.from_json = function(json) {
	return new PickMarker();
}

PickMarker.random = function(max_recursion) {
	return new PickMarker();
}

export { PickMarker, PICK_MARKER_TYPE };
