let PUT_MARKER_TYPE = "put_marker";

class PutMarker {
	constructor() {
		this.type = PUT_MARKER_TYPE;
	}

	get_code() {
		return "put_marker()";
	}

	transform(board, parameters, callback) {
		callback(this);
		if(parameters.max_budget <= 0)
			return;
		board.put_marker_at(board.get_robot_position());
		parameters.max_budget -= 1;
	}
}

PutMarker.from_json = function(json) {
	return new PutMarker();
}

PutMarker.random = function(max_recursion) {
	return new PutMarker();
}

export { PutMarker, PUT_MARKER_TYPE };
