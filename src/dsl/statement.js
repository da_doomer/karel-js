import { random_item } from "./dsl.js";
import { While } from "./while.js";
import { WHILE_TYPE } from "./while.js";
import { Repeat } from "./repeat.js";
import { REPEAT_TYPE } from "./repeat.js";
import { Sequence } from "./sequence.js";
import { SEQUENCE_TYPE } from "./sequence.js";
import { Action } from "./action.js";
import { ACTION_TYPES } from "./action.js";
import { If } from "./if.js";
import { IF_TYPE } from "./if.js";
import { IfElse } from "./ifelse.js";
import { IFELSE_TYPE } from "./ifelse.js";

class Statement {}

Statement.random = function(max_recursion) {
	if(max_recursion <= 1) {
		let classes = [Action];
		return random_item(classes).random(max_recursion-1);
	} else {
		let classes = [While, Repeat, Sequence, Action, If, IfElse];
		return random_item(classes).random(max_recursion-1);
	}
}

Statement.from_json = function(json) {
	if(json.type == WHILE_TYPE)
		return While.from_json(json);
	if(json.type == SEQUENCE_TYPE)
		return Sequence.from_json(json);
	if(ACTION_TYPES.includes(json.type))
		return Action.from_json(json);
	if(json.type == IF_TYPE)
		return If.from_json(json);
	if(json.type == IFELSE_TYPE)
		return IfElse.from_json(json);
}

export { Statement };
